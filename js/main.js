// App globals
var appScreenState = 'init';
var gAVplayObj;																														
var selectedRav = 0;
var selectedSubject = 0;
var selectedShiur = 0;
var rabanimList = [];
var subjectList = [];
var shiurimList = [];

// UI setup
$(document).ready(function(){		
	$.caph.focus.activate(function(nearestFocusableFinderProvider, controllerProvider) {
        controllerProvider.onFocused(function(event, originalEvent) {
        	//console.log("On rav: ",$(event.target).attr('rid'));	        	
   			var onItem = parseInt($(event.target).attr('rid'));
   			onFocusedItem(onItem);
        	$(event.currentTarget).css({
				border: '3px solid white'
			});
        });																																																									
   
        controllerProvider.onBlurred(function(event, originalEvent) {
        	$(event.currentTarget).css({
				border : '3px solid transparent'
			});
        });

 	    controllerProvider.onSelected(function(event, originalEvent){	 	    	
 	 		$(event.currentTarget).toggleClass("selected");
		});
    });	
});



function compare_by_count(a,b) {
	  if (a.Count > b.Count)
	    return -1;
	  if (a.Count < b.Count)
	    return 1;
	  return 0;
}


function setUpLoadingScreen() {    
    document.getElementById("rabanim").innerHTML = "<p class=\"loading_text\">טוען נתונים...</p>";
}

function setUpRabanimTiles() {    
    var rabanimHtmlTiles = ""
    for (var i = 0; i < rabanimList.length; i++) {
    		var tileTemplate = "<div focusable on-focused=\"fdldfocus($event)\" onselected=\"focus($event)\" rid=\"RAV_ID\" class=\"tile\"> \
    			<table ><tr><td><img src=\"IMAGE_SOURCE\"></td></tr><tr><td><div class=\"tile_title\">RAV_NAME</div></td></tr></table></div>"

    		var pic = "unknown"
    		if (rabanimList[i].Picture == "") {
	    		pic = "images/logo.jpg"
	    	}
	    	else {
	    		pic = rabanimList[i].Picture
	    	}
    		
    		rabanimHtmlTiles += tileTemplate.replace(/RAV_ID/g, i.toString()).replace(/IMAGE_SOURCE/g, pic).replace(/RAV_NAME/g, rabanimList[i].Name)
    	
    }    
    document.getElementById("rabanim").innerHTML = rabanimHtmlTiles
    appScreenState = 'rabbanim';
}

function get_rabanim()
{
	var x = new XMLHttpRequest();
	setUpLoadingScreen();
	x.open("GET", "http://www.meirtv.co.il/site/rss/rav.asp", true);
	x.onreadystatechange = function () {
		  if (x.readyState == 4 && x.status == 200)
		  {
		     var doc = x.responseXML;
		     var items = doc.getElementsByTagName("channel")[0].getElementsByTagName("item");
		     
		     for (var i = 0; i < items.length; i++) {
		    	 var name = items[i].getElementsByTagName("title")[0].textContent;
		    	 var count = parseInt(items[i].getElementsByTagName("counter")[0].textContent);
		    	 var id = parseInt(items[i].getElementsByTagName("id")[0].textContent);
		    	 var picture = items[i].getElementsByTagName("picture")[0].textContent;		    	 
		    	 var rav = { Name:name, Count:count, Id:id, Picture:picture };
		    	 rabanimList[i] = rav;
		     }
		     
		     rabanimList.sort(compare_by_count)
		     
		     console.log('Got rabanim');
		     
		     setUpRabanimTiles();
		  }
		};
	x.send(null);
}

function getSubjectsForRav(rav_id)
{
	var x = new XMLHttpRequest();
	setUpLoadingScreen();
	x.open("GET", "http://www.meirtv.co.il/site/rss/subject.asp?rabbi="+rav_id, true);
	x.onreadystatechange = function () {
		  var subjects = []
		  if (x.readyState == 4 && x.status == 200)
		  {
		     var doc = x.responseXML;
		     var items = doc.getElementsByTagName("channel")[0].getElementsByTagName("item");
		     
		     for (var i = 0; i < items.length; i++) {
		    	 var title = items[i].getElementsByTagName("title")[0].textContent;
		    	 var count = parseInt(items[i].getElementsByTagName("counter")[0].textContent);
		    	 var id = parseInt(items[i].getElementsByTagName("id")[0].textContent);		    	 		    	 
		    	 var subject = { Title:title, Count:count, Id:id };
		    	 subjects[i] = subject;
		     }
		     
		     subjects.sort(compare_by_count)
		     
		     subjectList = subjects
		     
		     console.log('Got subjects');
		     
		     setUpSubjectList(subjects);
		  }
		};
	x.send(null);
}

function setUpSubjectList(subjects) {    
    var subjectHtmlTiles = "";
    for (var i = 0; i < subjects.length; i++) {
    		var tileTemplate = "<div focusable on-focused=\"fdldfocus($event)\" onselected=\"focus($event)\" rid=\"SUBJECT_ID\" class=\"active_list\"> \
    			<div class=\"active_list_title\">SUBJECT_NAME</div></div>"
    		
    		subjectHtmlTiles += tileTemplate.replace(/SUBJECT_ID/g, i.toString()).replace(/SUBJECT_NAME/g, subjects[i].Title);
    	
    }    
    document.getElementById("rabanim").innerHTML = subjectHtmlTiles;
    appScreenState = 'subjects';
}

function getShiursForRavForSubject(rav_id,subject_id) {
	var x = new XMLHttpRequest();
	setUpLoadingScreen();
	x.open("GET", "http://www.meirtv.co.il/site/rss/lessons.asp?iPageSize=100&rabbi="+rav_id+"&cat_id="+subject_id, true);
	x.onreadystatechange = function () {
		  var shiurim = []
		  if (x.readyState == 4 && x.status == 200)
		  {
		     var doc = x.responseXML;
		     var items = doc.getElementsByTagName("channel")[0].getElementsByTagName("item");
		     
		     for (var i = 0; i < items.length; i++) {
		    	 var title = items[i].getElementsByTagName("title")[0].textContent;
		    	 var sub_title = items[i].getElementsByTagName("sub_title")[0].textContent;
		    	 var hdate = items[i].getElementsByTagName("dateup_hebrew")[0].textContent;
		    	 var id = parseInt(items[i].getElementsByTagName("idx")[0].textContent);
		    	 var length = parseInt(items[i].getElementsByTagName("length")[0].textContent);
		    	 var video_url = items[i].getElementsByTagName("video")[0].textContent;
		    	 var audio_url = items[i].getElementsByTagName("url")[0].textContent;
		    	 var shiur = { Title:title, SubTitle:sub_title, Length:length, Id:id, HebrewDate:hdate, VideoUrl:video_url };
		    	 shiurim[i] = shiur;
		     }
		     		     
		     shiurimList = shiurim
		     
		     console.log('Got shiurim');
		     
		     setUpShiurimList(shiurim);
		  }
		};
	x.send(null);
}

function setUpShiurimList(shiurim) {    
    var shiurimHtmlList = "";
    for (var i = 0; i < shiurim.length; i++) {
    		var tileTemplate = "<div focusable on-focused=\"fdldfocus($event)\" onselected=\"focus($event)\" rid=\"SUBJECT_ID\" class=\"active_list\"> \
    			<div class=\"active_list_title\">TITLE (LENGTH)</div></div>"
    		
    		shiurimHtmlList += tileTemplate.replace(/SUBJECT_ID/g, i.toString()).replace(/TITLE/g, shiurim[i].Title).replace(/LENGTH/g, shiurim[i].Length);
    	
    }    
    document.getElementById("rabanim").innerHTML = shiurimHtmlList;
    appScreenState = 'lessons';
}

function onPlaySuccess() {
    console.log("Playback started successfully ");
}

function onPlayError(error) {
    console.log(error.message);
  }

function switchPlayer() {
	 $("#av-player").css({
	    	display: 'block'
		});
	    $("#whole-page").css({
	    	display: 'none'
		});
}

function switchToPage() {
	 $("#av-player").css({
	    	display: 'none'
		});
	    $("#whole-page").css({
	    	display: 'block'
		});
}

function playShiur(shiur) {	
	//setUpLoadingScreen();
	console.log('Playing: ');
	console.log(shiur.VideoUrl);
	
	//gAVplayObj.init();    
    gAVplayObj.open(shiur.VideoUrl);        
    gAVplayObj.prepare();        
    gAVplayObj.setDisplayRect(0, 0, 1920, 1080);
    gAVplayObj.setDisplayMethod('PLAYER_DISPLAY_MODE_FULL_SCREEN');
    gAVplayObj.play();
    switchPlayer();
	appScreenState = 'playing';
}


function rav_selected(id) {
	console.log("Selected",id)
}


function onFocusedItem(item_id) {
	switch(appScreenState){
		case 'rabbanim':
			selectedRav = item_id;
			break;
		case 'subjects':
			selectedSubject = item_id;
			break;
		case 'lessons':
			selectedLesson = item_id;
	}
	
}

function onOkPressed() {
	switch(appScreenState){
		case 'rabbanim':
			console.log("Selected RAV:");
			console.log(selectedRav);
			getSubjectsForRav(rabanimList[selectedRav].Id);
			break;
		case 'subjects':
			console.log("Selected subject:");
			console.log(selectedSubject);
			getShiursForRavForSubject(rabanimList[selectedRav].Id,subjectList[selectedSubject].Id);
			break;
		case 'lessons':
			console.log("Selected shiur:");
			console.log(selectedSubject);
			playShiur(shiurimList[selectedLesson]);
			break;
	}
}

function onBackPressed() {
	switch(appScreenState){
		case 'rabbanim':
			tizen.application.getCurrentApplication().exit();
			break;
		case 'subjects':
			setUpRabanimTiles();
			break;
		case 'lessons':
			getSubjectsForRav(rabanimList[selectedRav].Id);			
			break;
		case 'playing':
			gAVplayObj.stop();
			switchToPage();
			getShiursForRavForSubject(rabanimList[selectedRav].Id,subjectList[selectedSubject].Id);
			break;
			
	}
}

function dir(object) {
    stuff = [];
    for (s in object) {
        stuff.push(s);
    }
    stuff.sort();
    return stuff;
}

//Initialize function
var init = function () {
    // TODO:: Do your initialization job
    console.log('init() called');
    
    document.addEventListener('visibilitychange', function() {
        if(document.hidden){
            // Something you want to do when hide or exit.
        } else {
            // Something you want to do when resume.
        }
    });
 
    // add eventListener for keydown
    document.addEventListener('keydown', function(e) {
    	switch(e.keyCode){
    	case 37: //LEFT arrow
    		break;
    	case 38: //UP arrow
    		break;
    	case 39: //RIGHT arrow
    		break;
    	case 40: //DOWN arrow
    		break;
    	case 13: //OK button
    		onOkPressed();
    		break;
    	case 10009: //RETURN button
    		onBackPressed();
    		break;
    	default:
    		console.log('Key code : ' + e.keyCode);
    		break;
    	}
    });
    
    console.log('avplaystore');
    console.log(dir(webapis.avplaystore));
    
    // Player object
    try {
    	gAVplayObj = webapis.avplaystore.getPlayer();
    	console.log('player');
        console.log(dir(gAVplayObj));
    } catch (error) {
        console.log(error.name);
    } 
    
    // Meir specific
    get_rabanim(); // this initiates tile setup
    
    
};
// window.onload can work without <body onload="">
window.onload = init;

